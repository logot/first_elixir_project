defmodule TestCase do
  # Callback invoked by `use`.
  #
  # For now it returns a quote expressions that
  # imports the module itself into the user code.
  @doc false
  defmacro __using__(_opts) do
    quote do
      import TestCase

      @test []
      @before_compile unquote(__MODULE__)
    end
  end

  @doc"""
  Defines a test case with the given description.

  ## Examples

      test "arithmetic operations" do
        4 = 2 + 2
      end

  """
  defmacro test(description, do: block) do
    function_name = String.to_atom("test " <> description)
    quote do
      @test [unquote(function_name) | @test]
      def unquote(function_name)(), do: unquote(block)
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      def run do
        Enum.each @test, fn test ->
          IO.puts "Running #{test}"
          apply(__MODULE__, test, [])
        end
      end
    end
  end
end

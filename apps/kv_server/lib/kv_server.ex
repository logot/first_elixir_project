defmodule KVServer do
  require Logger

  @moduledoc """
  Documentation for `KVServer`.
  """

  @doc """
  Accept port and listen for TCP connections.
  """
  def accept(port) do
    # listen socket
    {:ok, socket} =
      :gen_tcp.listen(port, [:binary, packet: :line, active: false, reuseaddr: true])

    Logger.info("Accepting connections on port #{port}")
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    # client socket (tcp connection)
    {:ok, client} = :gen_tcp.accept(socket)
    # serve(client)
    {:ok, pid} = Task.Supervisor.start_child(KVServer.TaskSupervisor, fn -> serve(client) end)
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end

  defp serve(socket) do
    # socket
    # |> read_line()
    # |> write_line(socket)
    #
    # serve(socket)
    msg =
      with {:ok, data} <- read_line(socket),
           {:ok, command} <- KVServer.Command.parse(data),
           do: KVServer.Command.run(command)

    # case read_line(socket) do
    #   {:ok, data} ->
    #     case KVServer.Command.parse(data) do
    #       {:ok, command} ->
    #         KVServer.Command.run(command)
    #
    #       {:error, _} = err ->
    #         err
    #     end
    #
    #   {:error, _} = err ->
    #     err
    # end

    write_line(socket, msg)
    serve(socket)
  end

  defp read_line(socket) do
    # this will be crashed if the socket has crached.
    # {:ok, line} = :gen_tcp.recv(socket, 0)
    # line
    :gen_tcp.recv(socket, 0)
  end

  # defp write_line(line, socket) do
  #   :gen_tcp.send(socket, line)
  # end
  defp write_line(socket, {:ok, text}) do
    :gen_tcp.send(socket, text)
  end

  defp write_line(socket, {:error, :unknown_command}) do
    # Known error; write to the client
    :gen_tcp.send(socket, "UNKNOWN COMMAND\r\n")
  end

  defp write_line(_socket, {:error, :closed}) do
    # The connection was closed, exit politely
    exit(:shutdown)
  end

  defp write_line(socket, {:error, :not_found}) do
    :gen_tcp.send(socket, "NOT FOUND\r\n")
  end

  defp write_line(socket, {:error, error}) do
    # Unknown error; write to the client and exit
    :gen_tcp.send(socket, "ERROR\r\n")
    exit(error)
  end
end

defmodule KV.Registry do
  use GenServer

  ## Client API
  @doc """
  Starts the registry with the given options

  `:name` is always required.
  """
  def start_link(opts) do
    # 1. pass the name to GenServer's init
    server = Keyword.fetch!(opts, :name)
    # GenServer.start_link(__MODULE__, :ok, opts)
    GenServer.start_link(__MODULE__, server, opts)
  end

  @doc """
  Looks up the bucket pid for `name` stored in `server`

  Returns `{:ok, pid}` if the bucket exists, `:error` otherwise.
  """
  def lookup(server, name) do
    # 2. Lookup is now done directly in ETS, without accessing the server
    # GenServer.call(server, {:lookup, name})
    case :ets.lookup(server, name) do
      [{^name, pid}] -> {:ok, pid}
      [] -> :error
    end
  end

  @doc """
  Ensures there is a bucket associated with the given`name` in `server`
  """
  def create(server, name) do
    # GenServer.cast(server, {:create, name})
    GenServer.call(server, {:create, name})
  end

  ## Defining GenServer Callbacks
  @impl true
  def init(table) do
    # 3. We have replaced the names map by the ETS table
    # for refs of monitor process
    # names = %{}
    names = :ets.new(table, [:named_table, read_concurrency: true])
    # for name
    refs = %{}
    # {:ok, %{}}
    {:ok, {names, refs}}
  end

  # 4. The previous handle_call callback for lookup was removed
  # @impl true
  # def handle_call({:lookup, name}, _from, state) do
  #   {names, _} = state
  #   {:reply, Map.fetch(names, name), state}
  # end

  @impl true
  def handle_call({:create, name}, _from, {names, refs}) do
    case lookup(names, name) do
      {:ok, pid} ->
        {:reply, {:ok, pid}, {names, refs}}

      :error ->
        {:ok, pid} = DynamicSupervisor.start_child(KV.BucketSupervisor, KV.Bucket)
        ref = Process.monitor(pid)
        refs = Map.put(refs, ref, name)
        :ets.insert(names, {name, pid})
        {:reply, {:ok, pid}, {names, refs}}
    end
  end

  @impl true
  def handle_cast({:create, name}, {names, refs}) do
    # 5. Read and write to the ETS table instead of the map
    # retrieve the bucket name in the names
    # if Map.has_key?(names, name) do
    #   # {:noreply, names}
    #   {:noreply, {names, refs}}
    # else
    #   # {:ok, bucket} = KV.Bucket.start_link([])
    #   # Many registry shared KV.BucketSupervisor
    #   {:ok, bucket} = DynamicSupervisor.start_child(KV.BucketSupervisor, KV.Bucket)
    #   ref = Process.monitor(bucket)
    #   refs = Map.put(refs, ref, name)
    #   names = Map.put(names, name, bucket)
    #   # add the bucket to the map as key-value
    #   # {:noreply, Map.put(names, name, bucket)}
    #   {:noreply, {names, refs}}
    # end
    case lookup(names, name) do
      {:ok, _pid} ->
        {:noreply, {names, refs}}

      :error ->
        {:ok, pid} = DynamicSupervisor.start_child(KV.BucketSupervisor, KV.Bucket)
        ref = Process.monitor(pid)
        refs = Map.put(refs, ref, name)
        :ets.insert(names, {name, pid})
        {:noreply, {names, refs}}
    end
  end

  @impl true
  def handle_info({:DOWN, ref, :process, _pid, _reason}, {names, refs}) do
    # 6. Delete from the ETS table instead of the map
    {name, refs} = Map.pop(refs, ref)
    # delete the bucket name if the process is exited.
    # names = Map.delete(names, name)
    :ets.delete(names, name)
    {:noreply, {names, refs}}
  end

  @impl true
  # for rest of the messages
  def handle_info(msg, state) do
    require Logger
    Logger.debug("Unexpected message in KV.Registry: #{inspect(msg)}")
    {:noreply, state}
  end
end

defmodule KV.Router do
  @doc """
  Dispatch the given `mod`, `fun`, `args` request to the appropriate node based on the `bucket`.

  Task.Superviosr.async({supervisor_name, node_name}, module_name, funciton_name, args: [bucket, mod, fun, args])
  """
  def route(bucket, mod, fun, args) do
    # Get the first byte of the binary of bucket name
    first = :binary.first(bucket)

    # Try to find an entry in the table() or raise
    entry =
      Enum.find(table(), fn {enum, _node} ->
        first in enum
      end) || no_entry_error(bucket)

    # If the entry node is the current node
    if elem(entry, 1) == node() do
      # execute on the current node
      apply(mod, fun, args)
    else
      # execute on the other node
      {KV.RouterTasks, elem(entry, 1)}
      |> Task.Supervisor.async(KV.Router, :route, [bucket, mod, fun, args])
      |> Task.await()
    end
  end

  defp no_entry_error(bucket) do
    raise "could not find entry for #{inspect(bucket)} in table #{inspect(table())}"
  end

  @doc """
  The routing table.
  """
  def table do
    Application.fetch_env!(:kv, :routing_table)
  end
end

# defmodule KV.BucketTest do
#   use ExUnit.Case, async: true

#   test "stores values by key" do
#     {:ok, bucket} = KV.Bucket.start_link([])
#     assert KV.Bucket.get(bucket, "milk") == nil

#     KV.Bucket.put(bucket, "milk", 3)
#     assert KV.Bucket.get(bucket, "milk") == 3
#   end
# end

defmodule KV.BucketTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, bucket} = start_supervised(KV.Bucket)
    %{bucket: bucket}
  end

  test "stores values by key", %{bucket: bucket} do
    assert KV.Bucket.get(bucket, "milk") == nil

    KV.Bucket.put(bucket, "milk", 3)
    assert KV.Bucket.get(bucket, "milk") == 3
  end

  test "are temporary workers" do
    assert Supervisor.child_spec(KV.Bucket, []).restart == :temporary
  end

  test "delete values by key", %{bucket: bucket} do
    assert KV.Bucket.get(bucket, "milk") == nil
    KV.Bucket.put(bucket, "milk", 3)
    KV.Bucket.delete(bucket, "milk")
    assert KV.Bucket.get(bucket, "milk") == nil
  end
end
